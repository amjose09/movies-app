export default {
    API_KEY: import.meta.env.VITE_API_KEY,
    API_URL: import.meta.env.VITE_API_URL,
    API_IMAGE_URL: import.meta.env.VITE_API_IMAGE_URL,
}