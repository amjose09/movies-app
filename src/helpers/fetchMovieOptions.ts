
export const movieOptions = ["IN THEATERS", "TOP MOVIES", "POPULAR"] as const;
export type MovieOption = typeof movieOptions[number];

export const fetchMovieOptions = {
    "POPULAR": "popular",
    "TOP MOVIES": "top_rated",
    "IN THEATERS": "now_playing",
} as Record<MovieOption, string>;