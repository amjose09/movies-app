import axios from "axios";
import env from "../utils/env";

export const apiMovies = axios.create({
    baseURL: env.API_URL,
    params: {
        api_key: env.API_KEY,
    }
})