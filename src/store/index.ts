import { createStore } from "vuex";
import actions from './actions';
import getters from "./getters";
import { Genre, Movie, MovieDetails } from "./interfaces";
import mutations from './mutations';

export interface State {
    movies: Movie[]
    topRatedMovies: Movie[]
    genres: Genre[]
    isLoadingMovies: boolean
    activeMovie: MovieDetails | null
    showModal: boolean
}

const state: State = {
    movies: [],
    topRatedMovies: [],
    genres: [],
    showModal: false,
    isLoadingMovies: false,
    activeMovie: null
}

export const store = createStore<State>({
    state,
    mutations,
    actions,
    getters
})