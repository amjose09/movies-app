
import { GetterTree } from 'vuex'
import { State } from '.'
import { Movie } from './interfaces'

export const GETTERS = {
    GET_MOVIE_GENRES: 'GET_MOVIE_GENRES',
}

export default {
    [GETTERS.GET_MOVIE_GENRES]: (state) => (movie: Movie) => {
        return state.genres
            .filter(genre => movie.genre_ids.includes(genre.id))
            .map(genre => genre.name)
    }
} as GetterTree<State, State>