import { ActionTree } from 'vuex'
import { State } from '.'
import { apiMovies } from '../api/apiMovies'
import { MovieOption, fetchMovieOptions } from '../helpers/fetchMovieOptions';
import { GenreResponse, MovieDetails, MoviesResponse, TrailersResponse } from './interfaces';
import { MUTATIONS } from './mutations';

export const ACTIONS = {
    GET_INITIAL_MOVIES: 'GET_INITIAL_MOVIES',
    GET_GENRES: 'GET_GENRES',
    FETCH_MOVIES: 'FETCH_MOVIES',
    GET_TRAILER: 'GET_TRAILER',
    GET_MOVIE: 'GET_MOVIE',
}

const actions: ActionTree<State, State> = {
    async [ACTIONS.GET_INITIAL_MOVIES]({ commit, dispatch }) {
        commit(MUTATIONS.SET_IS_LOADING_MOVIES, true)
        try {
            const { data: { results } } = await apiMovies.get<MoviesResponse>('/movie/upcoming')
            await dispatch(ACTIONS.FETCH_MOVIES, "IN THEATERS")
            await dispatch(ACTIONS.GET_GENRES)

            commit(MUTATIONS.SET_TOP_RATED_MOVIES, results)
        } catch (error) {
            console.log(error)
        } finally {
            commit(MUTATIONS.SET_IS_LOADING_MOVIES, false)
        }
    },
    async [ACTIONS.FETCH_MOVIES]({ commit }, type: MovieOption) {
        try {
            const real_type = fetchMovieOptions[type]
            const { data } = await apiMovies.get<MoviesResponse>(`movie/${real_type}`)
            commit(MUTATIONS.SET_MOVIES, data.results)
        } catch (error) {
            console.log(error)
            commit(MUTATIONS.SET_MOVIES, [])
        }
    },
    async [ACTIONS.GET_GENRES]({ commit }) {
        try {
            const { data: { genres } } = await apiMovies.get<GenreResponse>('/genre/movie/list')
            commit(MUTATIONS.SET_GENRES, genres)
        } catch (error) {
            console.log(error)
        }
    },
    async [ACTIONS.GET_TRAILER](_, id: number) {
        try {
            const { data } = await apiMovies.get<TrailersResponse>(`/movie/${id}/videos`)
            const trailer = data.results.find(
                trailer => trailer.type === 'Trailer'
            ) || data.results[0]
            return trailer
        } catch (error) {
            console.log(error)
        }
    },
    async [ACTIONS.GET_MOVIE]({ commit }, id: number) {
        commit(MUTATIONS.SET_SHOW_MODAL, true)
        try {
            const { data } = await apiMovies.get<MovieDetails>(`/movie/${id}`)
            commit(MUTATIONS.SET_ACTIVE_MOVIE, data)
        } catch (error) {
            console.log(error)
        }
    }
}

export default actions