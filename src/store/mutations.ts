import { MutationTree } from 'vuex'
import { State } from '.'
import { Genre, Movie, MovieDetails } from './interfaces'

export const MUTATIONS = {
    SET_MOVIES: 'SET_MOVIES',
    SET_TOP_RATED_MOVIES: 'SET_TOP_RATED_MOVIES',
    SET_IS_LOADING_MOVIES: 'SET_IS_LOADING_MOVIES',
    SET_ACTIVE_MOVIE: 'SET_ACTIVE_MOVIE',
    SET_GENRES: 'SET_GENRES',
    SET_SHOW_MODAL: 'SET_SHOW_MODAL'
}

const mutations: MutationTree<State> = {
    [MUTATIONS.SET_MOVIES](state, movies: Movie[]) {
        state.movies = movies
    },
    [MUTATIONS.SET_IS_LOADING_MOVIES](state, isLoading: boolean) {
        state.isLoadingMovies = isLoading
    },
    [MUTATIONS.SET_TOP_RATED_MOVIES](state, movies: Movie[]) {
        state.topRatedMovies = movies
    },
    [MUTATIONS.SET_GENRES](state, genres: Genre[]) {
        state.genres = genres
    },
    [MUTATIONS.SET_ACTIVE_MOVIE](state, movie: MovieDetails | null) {
        state.activeMovie = movie
    },
    [MUTATIONS.SET_SHOW_MODAL](state, showModal: boolean) {
        state.showModal = showModal
    }
}

export default mutations