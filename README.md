# Movies App - Frontend test (Double V Partners)

#### This is a simple app that allows you to search for movies and view their details.

## Getting Started

- To get started, clone the repository and run `yarn` to install the dependencies.
- Run `yarn dev` to start the development server.
- Run `yarn build` to build the app for production.

## Stack
- Vue 3
- Vuex 
- Sass

## Deployed Version
- [Demo Here](https://movies-app-pbpe.vercel.app/)

## By 
### [Jose Luis Angarita](https://github.com/Jocanm)